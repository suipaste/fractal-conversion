﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Media;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true; //SMT
            pictureBox1.MouseDown += new MouseEventHandler(pictureBox1_MouseDown); // SMT
            pictureBox1.MouseMove += new MouseEventHandler(pictureBox1_MouseMove); // SMT
            pictureBox1.MouseUp += new MouseEventHandler(pictureBox1_MouseUp); // SMT

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            init();
            start();
        }

        // Convert from hue, saturation brightness to red, green, blue
        class HSB
        {//djm added, it makes it simpler to have this code in here than in the C#
            public float rChan, gChan, bChan;
            public HSB()
            {
                rChan = gChan = bChan = 0;
            }
            public void fromHSB(float h, float s, float b)
            {

                float red = b;
                float green = b;
                float blue = b;
                if (s != 0)
                {
                    float max = b;
                    float dif = b * s / 255f;
                    float min = b - dif;

                    float h2 = h * 360f / 255f;

                    if (h2 < 60f)
                    {
                        red = max;
                        green = h2 * dif / 60f + min;
                        blue = min;
                    }
                    else if (h2 < 120f)
                    {
                        red = -(h2 - 120f) * dif / 60f + min;
                        green = max;
                        blue = min;
                    }
                    else if (h2 < 180f)
                    {
                        red = min;
                        green = max;
                        blue = (h2 - 120f) * dif / 60f + min;
                    }
                    else if (h2 < 240f)
                    {
                        red = min;
                        green = -(h2 - 240f) * dif / 60f + min;
                        blue = max;
                    }
                    else if (h2 < 300f)
                    {
                        red = (h2 - 240f) * dif / 60f + min;
                        green = min;
                        blue = max;
                    }
                    else if (h2 <= 360f)
                    {
                        red = max;
                        green = min;
                        blue = -(h2 - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        red = 0;
                        green = 0;
                        blue = 0;
                    }
                }

                rChan = (float)Math.Round(Math.Min(Math.Max(red, 0), 255));
                gChan = (float)Math.Round(Math.Min(Math.Max(green, 0), 255));
                bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255));

            }
        }



        // Define Variables
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        //private Image picture; SMT -- Now replaced with picture box
        private Bitmap bmpFractal; //SMT
        private Bitmap bmpZoomBox; //SMT
        private Graphics gFractal; //SMT - refactored
        private Graphics gZoomBox; //SMT
        private bool mousePressed; //SMT
        private bool colorCyclingOn = false; // SMT
        private ColorPalette pal;


        //private Bitmap b = new Bitmap(100, 100);
        //private Cursor c1, c2;
        private HSB HSBcol;

        public void init() // all instances will be prepared
        {
            HSBcol = new HSB();
            //setSize(640, 480); NOT REQUIRED
            finished = false;
            //addMouseListener(this);
            //addMouseMotionListener(this);
            //c1 = new Cursor(Cursor.WAIT_CURSOR);
            //c2 = new Cursor(Cursor.CROSSHAIR_CURSOR);
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            bmpFractal = new Bitmap(x1, y1);
            bmpZoomBox = new Bitmap(x1, y1);
            gFractal = Graphics.FromImage(bmpFractal);
            gZoomBox = Graphics.FromImage(bmpZoomBox); // SMT
            pal = bmpFractal.Palette;
            finished = true;

        }

        public void destroy() // delete all instances 
        {
            if (finished == true)
            {
                bmpFractal = null;
                gFractal = null;
                //System.gc(); // garbage collection
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        public void update()
        {
            gZoomBox.Clear(Color.Transparent); // SMT clear the previous drag box

            if (rectangle)
            {
                Color recCol = new Color();
                recCol = Color.White;
                Pen recPen = new Pen(recCol);
                if (xs < xe)
                {
                    if (ys < ye) gZoomBox.DrawRectangle(recPen, xs, ys, (xe - xs), (ye - ys));
                    else gZoomBox.DrawRectangle(recPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) gZoomBox.DrawRectangle(recPen, xe, ys, (xs - xe), (ye - ys));
                    else gZoomBox.DrawRectangle(recPen, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            Color col = new Color();

            action = false;

            for (x = 0; x < x1; x += 2)
            {
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value

                    if (h != alt)
                    {
                        b = (1.0f - h * h); // brightness
                        HSBcol.fromHSB(h * 255, 0.8f * 255, b * 255); //convert hsb to rgb then make a Java Color

                        col = Color.FromArgb(255, (int)Math.Round(HSBcol.rChan), (int)Math.Round(HSBcol.gChan), (int)Math.Round(HSBcol.bChan));

                        alt = h;
                    }
                    Pen pen = new Pen(col);
                    gFractal.DrawLine(pen, x, y, x + 1, y);
                }
            }
            action = true;

        }


        private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) // Detects if the left mouse button has been pressed and takes the screen location
        {
            mousePressed = true;

            xs = e.X;
            ys = e.Y;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) // Detects if the left mouse button has been released - this triggers zooming and redrawing of the fractal
        {
            mousePressed = false;
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                gZoomBox.Clear(Color.Transparent);
                pictureBox1.Refresh();
            }
        }

        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) // Detects if the mouse has moved and takes its current position
        {
            if (mousePressed == true)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                update();
                pictureBox1.Refresh();
            }
        }

        private void repaint(object sender, PaintEventArgs e) // Repaint the picturebox
        {         
                e.Graphics.DrawImage(bmpFractal, 0, 0);
            e.Graphics.DrawImage(bmpZoomBox, 0, 0);
            
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) // new image menu button
        {
            init();
            start();
            pictureBox1.Refresh();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e) // copy image to clipboard
        {
            Clipboard.SetImage(bmpFractal);
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e) // Save the fractal image
        {
            SaveFileDialog dialog = new SaveFileDialog(); // set the defaults for the save dialog
            dialog.Filter = "PNG|*.PNG";
            dialog.DefaultExt = "PNG|*.PNG";
            dialog.FileName = "Fractal";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                bmpFractal.Save(dialog.FileName, ImageFormat.Png);
            }

        }

        private void flipXToolStripMenuItem_Click(object sender, EventArgs e) // Flip the fractal image on the X axis
        {
            bmpFractal.RotateFlip(RotateFlipType.Rotate180FlipX);
            pictureBox1.Refresh();
        }

        private void flipYToolStripMenuItem_Click(object sender, EventArgs e) // Flip the fractal image on the Y axis
        {
            bmpFractal.RotateFlip(RotateFlipType.Rotate180FlipY);
            pictureBox1.Refresh();
        }

        private void enableColorCyclingToolStripMenuItem_Click(object sender, EventArgs e) // enable color cycling (on/off)
        {

            if (colorCyclingOn == false)
            {
                colorCyclingOn = true;
            }
            else
            {
                colorCyclingOn = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e) // change palette colour
        {
            if (colorCyclingOn == true)
            {
                //Create new image
                Image colourCycle = changeColor(bmpFractal);
                gFractal = Graphics.FromImage(bmpFractal);

                // replace fractal image with cycled image
                gFractal.DrawImage(colourCycle, 0, 0);

                pictureBox1.Refresh();
            }
        }

        private static Bitmap changeColor(Bitmap original)
        {
            var bitMapStream = new MemoryStream(); //create a new memory stream called bitMapStream - Used for data transefers, Compiling resources
            original.Save(bitMapStream, ImageFormat.Gif); //Save the original bitmap as a gif image
            var newBitmap = new Bitmap(bitMapStream);//A newBitmap variable is set to the bitmap saved previously in the image stream

            //Create a copy of the color palette to modify
            var newPalette = newBitmap.Palette; //create a palette and set the newBitmap to the palette (A Palette Image)

            for (var i = 0; i < newPalette.Entries.Length - 6; i++) //Loop through all of the entries in the colour palette array
            {
                Color oldColor = newPalette.Entries[i]; //Replace the value in the Entries array with the next value in the entries array
                newPalette.Entries[i] = newPalette.Entries[i + 1]; //Replace the value in the Entries array with the next value in the entries array
                newPalette.Entries[i + 1] = oldColor;
            }

            newBitmap.Palette = newPalette; //set the bitmap to the new modified bitmap
            return newBitmap; //return the new Bitmap image
        }


        /*public Color FromHsv(double hue, double saturation, double value) // For colour cycling 
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);
            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }*/

        private void saveStateToolStripMenuItem_Click(object sender, EventArgs e) // Save fractal state
        {
            //File.WriteAllText(@"C:\Users\c3287215\Desktop\WriteText.txt", String.Empty);

            string strX1 = Convert.ToString(x1);
            string strY1 = Convert.ToString(y1);
            string strXs = Convert.ToString(xs);
            string strYs = Convert.ToString(ys);
            string strXe = Convert.ToString(xe);
            string strYe = Convert.ToString(ye);
            string strXstart = Convert.ToString(xstart);
            string strYstart = Convert.ToString(ystart);
            string strXend = Convert.ToString(xende);
            string strYend = Convert.ToString(yende);
            string strXzoom = Convert.ToString(xzoom);
            string strYzoom = Convert.ToString(yzoom);

            string[] array = { strX1, strY1, strXs, strYs, strXe, strYe, strXstart, strYstart, strXend, strYend, strXzoom, strYzoom }; // create an array of required values

            SaveFileDialog save = new SaveFileDialog(); // output array to txt file
            save.FileName = "FractalState.txt";
            save.Filter = "Text File | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(save.OpenFile());
                for (int i = 0; i < 12; i++)
                {
                    writer.WriteLine(array[i].ToString());
                }
                writer.Dispose();
                writer.Close();
            }
        }

        private void loadStateToolStripMenuItem_Click(object sender, EventArgs e) // Load fractal state
        {
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string[] lines = System.IO.File.ReadAllLines(openFileDialog1.FileName);

                x1 = Convert.ToInt32(lines[0]);
                y1 = Convert.ToInt32(lines[1]);
                xs = Convert.ToInt32(lines[2]);
                ys = Convert.ToInt32(lines[3]);
                xe = Convert.ToInt32(lines[4]);
                ye = Convert.ToInt32(lines[5]);
                xstart = Convert.ToDouble(lines[6]);
                ystart = Convert.ToDouble(lines[7]);
                xende = Convert.ToDouble(lines[8]);
                yende = Convert.ToDouble(lines[9]);
                xzoom = Convert.ToDouble(lines[10]);
                yzoom = Convert.ToDouble(lines[11]);


                mandelbrot();
                pictureBox1.Refresh();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

    }
}
